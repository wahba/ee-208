.equ state_closed = 0
.equ state_opened = 1
.equ state_edit = 2

.equ btn_validate = 0x10 ; btn vol+
.equ btn_edit = 0x11 ; btn vol-
.equ btn_close = 0x0c ; btn off
.equ btn_abort = 0x38 ; btn av
.equ btns_move = 0x30	;btn ch+/-
.equ move_forward = 0x00 ; btn ch+
.equ move_backwards = 0x01 ; btn ch-

.equ digit_code_max = 0x09
.equ digit_code_min = 0x00

.equ SDA_port = PORTD
.equ SDA_pin = SDA
.equ SCL_port = PORTD
.equ SCL_pin = SCL

.equ NOCODE = 0xff
.equ NOCHAR = 0x00
.equ ASCII_DIGIT_OFFSET = '0'