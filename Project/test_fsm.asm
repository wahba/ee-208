.include "macros.asm"
.include "definitions.asm"
.include "utils.asm"
.include "constants.asm"

.dseg
state: .byte 1
cin: .byte 1

.cseg
.org 0
		rjmp reset
.org INT7addr
		rjmp handle_remote

.include "lcd.asm"
.include "printf.asm"


reset:	LDSP RAMEND
		rcall LCD_init
		SET_VAR_IM state, state_closed
		
		OUTI DDRB, 0xff

		OUTI EIMSK, (1 << INT7)

		sei
		rjmp main

main:	GET_VAR w, state
		out PORTB, w
			
		cpi w, state_closed
		brne lo
		push w
		rcall loop_closed
		pop w

lo:		cpi w, state_opened
		brne le
		push w
		rcall loop_opened
		pop w

le:		cpi w, state_edit
		brne re
		push w
		rcall loop_edit
		pop w

re:		cli
		SET_VAR_IM cin, NOCODE
		sei
		rjmp main

loop_closed:
		GET_VAR w, cin

		cpi w, btn_close
		brne lcbp
		SET_VAR_IM state, state_opened

lcbp:	
		ret

loop_opened:
		GET_VAR w, cin

		cpi w, btn_close
		brne lobp
		SET_VAR_IM state, state_edit
lobp:	
		ret

loop_edit:
		GET_VAR w, cin

		cpi w, btn_close
		brne lebp
		SET_VAR_IM state, state_closed

lebp:	
		ret

.equ T1 = 1778

handle_remote:
		in _sreg, SREG
		push w
		CLR2 b1, b0
		ldi b2, 14
		WAIT_US T1/4

hr_loop:P2C PINE, IR
		ROL2 b1, b0
		WAIT_US T1
		DJNZ b2, hr_loop
		
		SET_VAR cin, b0
		pop w
		out SREG, _sreg

		reti