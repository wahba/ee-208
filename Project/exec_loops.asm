loop_closed:
		; on digit : append to password
		JMPCINR lcv, digit_code_min, digit_code_max
		rcall append_pswd
		rcall render_pswd

		; on validate : check against
lcv:	JMNPCI lcr, btn_validate
		rcall check_pswd ; sets T = 1 if wrong password
		brts lcbp
		rcall state_200_sound
		rcall render_opened_msg
		rcall open_door
		SET_VAR_IM state, state_opened
		rjmp lcnd
lcbp:	rcall state_400_sound
		rcall render_closed_msg
lcnd:	rcall clear_pswd
lcr:	ret

loop_opened:
		; on edit : state <- edit
loe:	JMNPCI loc, btn_edit
		rcall state_200_sound
		rcall render_edit_msg
		SET_VAR_IM state, state_edit

		; on close : state <- close
loc:	JMNPCI lor, btn_close
		rcall state_200_sound
		rcall render_closed_msg
		rcall close_door
		SET_VAR_IM state, state_closed

lor:	ret

loop_edit:
		; on digit : append to password
		JMPCINR lev, digit_code_min, digit_code_max
		rcall append_pswd
		rcall render_pswd

		; on validate : change password + state <- opened
lev:	JMNPCI lea, btn_validate
		rcall change_pswd
		brts levbp
		rcall state_200_sound
		rjmp PC+2
levbp:	rcall state_400_sound
		SET_VAR_IM state, state_opened
		rcall clear_pswd
		rcall render_opened_msg

		; on abort :  state <- opened
lea:	JMNPCI ler, btn_abort
		rcall clear_pswd
		rcall render_opened_msg
		rcall state_400_sound
		SET_VAR_IM state, state_opened

ler:	ret