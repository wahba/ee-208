;	===== STACK INTERACTIONS ====
.macro PUSHI; stack <-k(0)
		ldi w, @0
		push w
		.endmacro

.macro SET_RET_TO_MAIN; addr(0)
		pop w
		pop w
		PUSHI low(main)
		PUSHI high(main)
		.endmacro

;	===== VARS INTERACTIONS ====

.macro GET_VAR; reg(0) <- var(0)
		lds @0, @1
		.endmacro

.macro SET_VAR; var(0) <- reg(1)
		sts @0, @1
		.endmacro

.macro SET_VAR_IM; var(0) <- k(1)
		ldi w, @1
		SET_VAR @0, w
		.endmacro

;	===== CURRENT IR INPUT OPERATIONS ====

.macro JMNPCI; jump to addr(0) iff cin is not k(1)
		GET_VAR w, cin
		cpi w, @1
		brne @0
		.endmacro

.macro JMPCINR; jump to addr(0) iff cin is not in range [low(1); high(2)]
		GET_VAR w, cin
		cpi w, @1
		breq bpl
		subi w, @1; check higher than lower bound
		brmi @0

bpl:	GET_VAR w, cin
		cpi w, @2
		breq bph
		subi w, @2 + 1 ; check smaller than higher bound
		brpl @0
bph:
	.endmacro

; ============= MISC ===============

.macro CA3		;call a subroutine with three arguments in a1:a0 b0
		ldi	a0, low(@1)		;speed and rotation direction
		ldi a1, high(@1)	;speed and rotation direction
		ldi b0, @2			;angle
		rcall	@0
.endmacro

.macro BLS ; branch to state loop(1) if state is (0), otherwise, branch to (2)
		cpi w, @0
		brne @2
		push w
		rcall @1
		pop w
		.endmacro