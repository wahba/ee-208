.include "macros.asm"
.include "definitions.asm"
.include "utils.asm"
.include "constants.asm"

.cseg
.org 0
		rjmp reset

.include "twi.asm"
.include "lcd.asm"

reset:	LDSP RAMEND

		OUTEI TWBR, 12
		OUTEI TWSR, 0x00
		in w, SFIOR
		ori w, (1 << PUD)
		out SFIOR, w

		ldi w, (1<<TWINT) | (1 << TWSTO) | (1<<TWEN)
		sts TWCR, w

		rjmp main

main:	
		rcall twi_startc
		CWAI twi_sla_address_mtwr, SLA_W
		CWAI twi_dataWR_ack, 0x00
		CWAI twi_dataWR_ack, 0x00

		CWAI twi_dataWR_ack, '0'
		CWAI twi_dataWR_ack, '0'
		CWAI twi_dataWR_ack, '0'
		CWAI twi_dataWR_ack, '0'

		CWAI twi_dataWR_ack, '0'
		CWAI twi_dataWR_ack, '0'
		CWAI twi_dataWR_ack, '0'
		CWAI twi_dataWR_ack, '0'
		rcall twi_stopc


		rjmp PC

twi_end:rcall twi_stopc
		OUTI PORTB, 0xAA
		rjmp PC