;
; Project.asm


.include "macros.asm"
.include "definitions.asm"

.include "constants.asm"
.include "utils.asm"

.dseg
state:.byte 1; closed = default
cin: .byte 1 ; current input being treated 

.cseg
.org 0
		rjmp reset

.org INT7addr
		rjmp handle_remote

; ============= RESET ==================

.org 0x100
reset:  LDSP RAMEND

		; output for servo
		OUTI DDRB, 0xff

		; init & clear password (in sram)
		SET_VAR_IM pswd_char_num, 0x00
		SET_VAR_IM cin, NOCODE

		rcall clear_pswd
		
		; close the door if it needs to be closed
		GET_VAR w, state
		cpi w, state_closed 
		
		; reset forces state = closed
		SET_VAR_IM state, state_closed
		SET_VAR_IM cin, NOCODE

		; setup eeprom
		OUTI DDRD, 0xff
		OUTI PORTD, 0xff

		OUTEI TWBR, 12
		OUTEI TWSR, 0x00
		in w, SFIOR
		ori w, (1 << PUD)
		out SFIOR, w

		ldi w, (1<<TWINT) | (1 << TWSTO) | (1<<TWEN)
		sts TWCR, w

		; enable interrupts
		OUTI EIMSK, (1 << INT7) ; IR on low levels
		
		; setup LCD
		rcall LCD_init
		rcall LCD_clear
		rcall render_closed_msg

		; enable speaker
		sbi	DDRE,SPEAKER

		sei
		rjmp main

; ============ INTERRUPT =================

.equ T1 = 1778

handle_remote:
		in _sreg, SREG
		push w
		CLR2 b1, b0
		ldi b2, 14
		WAIT_US T1/4

hr_loop:P2C PINE, IR
		ROL2 b1, b0
		WAIT_US T1
		DJNZ b2, hr_loop
		
		SET_VAR cin, b0
		pop w
		out SREG, _sreg

		reti

; ========== PRE INCULSIONS ==========

.include "lcd.asm"
.include "printf.asm"
.include "twi.asm"

; ============== MAIN LOOP ================

main:	GET_VAR w, state
		
mlc:	BLS state_closed, loop_closed, mlo
mlo:	BLS state_opened, loop_opened, mle
mle:	BLS state_edit, loop_edit, mre

mre:	cli
		WAIT_US 10				; experimentally noticed improvement
		SET_VAR_IM cin, NOCODE
		sei
		rjmp main

; ============ HANDLER SUBS ==============

.include "exec_loops.asm"
.include "sound_effects.asm"
.include "pswd.asm"
.include "render.asm"
.include "door.asm"

; ============ FALLBACK ============
twi_end:rcall twi_stopc
		rjmp PC
