.include "macros.asm"
.include "definitions.asm"
.include "utils.asm"
.include "buffer.asm"
.include "constants.asm"

reset:	LDSP RAMEND
		ldi a0, 17
		BUFFER_PUSH a0
		lds b1, buff_n
		ldi a0, 19
		BUFFER_PUSH a0
		lds b1, buff_n

		ldi a0, 1
		BUFFER_PUSH a0
		lds b1, buff_n

		rjmp main

main:	
		BUFFER_POP b0
		lds b1, buff_n
		LDSY buff_out
		mov b2, yh
		mov b3, yl
		CLR2 yh, yl
		rjmp main