.include "definitions.asm"
.include "macros.asm"
.include "constants.asm"
.include "utils.asm"
.include "i2c_eeprom.asm"

.dseg
state: .byte 1
cin: .byte 1

.cseg
.org 0
	rjmp reset

.org INT7addr
		rjmp handle_remote

.include "i2c_subroutines.asm"
.include "lcd.asm"
.include "printf.asm"
.include "pswd.asm"
.include "render.asm"



reset:	LDSP RAMEND
		OUTI DDRB, 0xff
		OUTI EIMSK, (1 << INT7)

		SET_VAR_IM pswd_char_num, state_closed
		SET_VAR_IM cin, NOCODE

		rcall clear_pswd

		rcall LCD_init
		rcall LCD_clear
		sei
		rjmp main

main:	
		GET_VAR w, cin
		cpi w, NOCODE			; sim actual loop
		breq bp					;

		rcall append_pswd
		rcall render_pswd

bp:		cli						; idem
		SET_VAR_IM cin, NOCODE	;
		sei						;
		rjmp main


.equ T1 = 1778

handle_remote:
		in _sreg, SREG
		push w
		CLR2 b1, b0
		ldi b2, 14
		WAIT_US T1/4

hr_loop:P2C PINE, IR
		ROL2 b1, b0
		WAIT_US T1
		DJNZ b2, hr_loop
		
		SET_VAR cin, b0
		pop w
		out SREG, _sreg

		reti