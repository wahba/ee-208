.equ PSWD_LEN = 8

.dseg
pswd_char_num: .byte 1
pswd: .byte PSWD_LEN ; pswd is 8 char max

.cseg
; ========= w/o peripherals ==========

clear_pswd:
		clr w
		LDI2 yh, yl, pswd

clpl:	push w			; save offset	
		ldi w, NOCHAR	; clear char
		st y+, w
		pop w			; restore offset
		inc w			; check if done
		cpi w, PSWD_LEN
		brne clpl
		
		SET_VAR_IM pswd_char_num, 0

		ret

append_pswd:
		PUSH2 a0, a1				; save a0, a1
		lds w, pswd_char_num		; check if end of pswd is reached
		cpi w, PSWD_LEN
		breq apr
		
		LDIY pswd
		ADDY w

		GET_VAR w, cin
		subi w, -ASCII_DIGIT_OFFSET ; ascii offset
		st y, w
								
		INCS pswd_char_num

apr:	POP2 a0, a1					; restore a0, a1
		ret

; ========= w/ peripherals ==========

change_pswd:
		clt		
		lds w, pswd_char_num	; check size = PSWD_LEN
		cpi w, PSWD_LEN
		breq PC+3
		set
		ret

		LDIY pswd
		push a1						; save a1

		rcall twi_startc			; init eeprom connection
		CWAI twi_sla_address_mtwr, SLA_W
		CWAI twi_dataWR_ack, 0x00
		CWAI twi_dataWR_ack, 0x00
			
		clr w

chgpl:	ld a1, y+					; load from sram
		push w						
		CWA twi_dataWR_ack, a1		; write to eeprom
		out PORTB, a1
		pop w

		inc w						; check if end of pswd is reached
		cpi w, PSWD_LEN
		brne chgpl

		rcall twi_stopc
		pop a1						; restore a1
		ret

check_pswd:
		clt
		LDIY pswd
		PUSH2 a1, a0				; save a1, b0

		rcall twi_startc			; init eeprom connection
		CWAI twi_sla_address_mtwr, SLA_W
		CWAI twi_dataWR_ack, 0x00
		CWAI twi_dataWR_ack, 0x00
		rcall twi_repstartc

		CWAI twi_sla_address_mtrd, SLA_R
		clr w 

chkpl:	ld a1, y+					; load from sram (input)
		push w
		rcall twi_dataRD_ack		; load from eeprom (ref)
		pop w
			
		cp a1, a0					; compare & stop if different
		breq chklb
		set
		rjmp chkpr

chklb:	inc w						; check if end of pswd is reached
		cpi w, PSWD_LEN
		brne chkpl
		
chkpr:	rcall twi_dataRD_noack
		rcall twi_stopc
		POP2 a1, a0					; restore a1, b0
		ret