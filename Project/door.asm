close_door:
		P0	PORTB,SERVO1			; pin=0
		CA3	servo_move, 1545, 0x36
		ret

open_door:
		P0	PORTB,SERVO1			; pin=0
		CA3	servo_move, 1490, 0x36
		ret

servo_move:
		cli
sml:	rcall	servoreg_pulse
		dec		b0
		brne	sml
		sei
		ret

; servoreg_pulse, in a1,a0, out servo port, mod a3,a2
; purpose generates pulse of length a1,a0
servoreg_pulse:
		WAIT_US	20000
		MOV2 a3,a2, a1,a0
		P1	PORTB,SERVO1		; pin=1	
sprl:	DEC2 a3,a2
		brne sprl
		P0	PORTB,SERVO1		; pin=0
		ret