.include "macros.asm"
.include "definitions.asm"

.org 0
	rjmp reset

.org INT7addr
		rjmp handle_remote

.equ T1 = 1778
reset:	LDSP RAMEND
		OUTI EIMSK, (1 << INT7)
		OUTI DDRB, 0xFF
		rcall LCD_init
		sei
		rjmp main

.include "printf.asm"

.include "lcd.asm"

main:	WAIT_MS 500
		in w, PORTB
		com w
		out PORTB, w
		rjmp main

handle_remote:
		in _sreg, SREG
		push w
		CLR2 b1, b0
		ldi b2, 14
		WAIT_US T1/4

loop:	P2C PINE, IR
		ROL2 b1, b0
		WAIT_US T1
		DJNZ b2, loop

		rcall LCD_home
		PRINTF LCD
		.db "cmd=", FHEX, b, 0, 0

		pop w
		out SREG, _sreg
		reti