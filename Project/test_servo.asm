; file	servo36218.asm   target ATmega128L-4MHz-STK300
; purpose 360-servo motor control as a classical 180-servo
; with increased angle capability
; module: M4, P7 servo Futaba S3003, output port: PORTB
.include "macros.asm"		; macro definitions
.include "definitions.asm"	; register/constant definitions

reset:
	LDSP	RAMEND			; set up stack pointer (SP)
	OUTI	DDRB,0xff		; configure portB to output
	rjmp	main			; jump to the main program

.macro CA3		;call a subroutine with three arguments in a1:a0 b0
	ldi	a0, low(@1)		;speed and rotation direction
	ldi a1, high(@1)	;speed and rotation direction
	ldi b0, @2			;angle
	rcall	@0
.endmacro

; main -----------------
main:	
	P0	PORTB,SERVO1			; pin=0
	LDI2	a1,a0,1500

ang_rot:						; fsm, utilization codes at locations t7:t0
	in		r23,PIND

t0:	cpi		r23,0b11111110
	brne	t3
	CA3	_s360, 1545, 0x36	; cw 90, low-speed

t3:	cpi		r23,0b01111111
	brne	t7

	CA3 _s360, 1485, 0x36	; ccw 90, low-speed
t7:	rjmp ang_rot
	
; _s360, in a1:a0, a2 out void, mod a2,w
; purpose execute arbitrary rotation
_s360:	
ls3601:
	rcall	servoreg_pulse
	dec		b0
	brne	ls3601
	ret

; servoreg_pulse, in a1,a0, out servo port, mod a3,a2
; purpose generates pulse of length a1,a0
servoreg_pulse:
	WAIT_US	20000
	MOV2	a3,a2, a1,a0
	P1	PORTB,SERVO1		; pin=1	
lpssp01:	DEC2	a3,a2
	brne	lpssp01
	P0	PORTB,SERVO1		; pin=0
	ret