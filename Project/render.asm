render_pswd:
		rcall LCD_clear
		LDIY pswd
		ldi w, PSWD_LEN
lbl:	ld a0, y+
		tst a0
		breq done
		push w
		rcall LCD_putc
		pop w
		dec w
		brne lbl
done:	ret

render_closed_msg:
	rcall LCD_clear
	PRINTF LCD
	.db "Enter Password", LF
	PRINTF LCD
	.db "+ OK", 0 
	ret

render_opened_msg:
	rcall LCD_clear
	PRINTF LCD
	.db "- Edit", "   ", "Close O", LF, 0
	ret

render_edit_msg:
	rcall LCD_clear
	PRINTF LCD
	.db "New Password", LF
	PRINTF LCD
	.db "+ OK", "   ", "Cancel AV", 0

	ret