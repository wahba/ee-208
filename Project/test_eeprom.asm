.include "macros.asm"
.include "definitions.asm"
.include "utils.asm"
.include "constants.asm"

.dseg
state: .byte 1
cin: .byte 1

.cseg
.org 0
		rjmp reset
	
.org INT7addr
		rjmp handle_remote

.include "twi.asm"
.include "lcd.asm"

reset:	LDSP RAMEND
		OUTI DDRB, 0xff
		OUTI PORTB, 0xff

		OUTI EIMSK, (1 << INT7)

		SET_VAR_IM pswd_char_num, state_closed
		SET_VAR_IM cin, NOCODE

		OUTI DDRD, 0xff
		OUTI PORTD, 0xff

		OUTEI TWBR, 12
		OUTEI TWSR, 0x00
		in w, SFIOR
		ori w, (1 << PUD)
		out SFIOR, w

		ldi w, (1<<TWINT) | (1 << TWSTO) | (1<<TWEN)
		sts TWCR, w

		rcall clear_pswd

		rcall LCD_init
		rcall LCD_clear
		sei
		rjmp main

main:	
		GET_VAR w, cin

		JMPCINR ch, digit_code_min, digit_code_max
		rcall append_pswd
		rcall render_pswd

ch:		JMNPCI bp, btn_validate

		rcall check_pswd ; sets T = 1 if wrong password
		rcall LCD_clear
		clr w
		bld w, 0
		push w

		rcall clear_pswd
		rcall LCD_clear
		pop w

		bst w, 0
		brts bp
		OUTI PORTB, 0x7E

bp:		cli						; idem
		SET_VAR_IM cin, NOCODE	;
		sei						;
		rjmp main

.equ T1 = 1778

handle_remote:
		in _sreg, SREG
		push w
		CLR2 b1, b0
		ldi b2, 14
		WAIT_US T1/4

hr_loop:P2C PINE, IR
		ROL2 b1, b0
		WAIT_US T1
		DJNZ b2, hr_loop
		
		SET_VAR cin, b0
		pop w
		out SREG, _sreg

		reti

.include "pswd.asm"
.include "render.asm"

twi_end:rcall twi_stopc
		OUTI PORTB, 0xAA
		rjmp PC