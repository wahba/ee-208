.include "macros.asm"
.include "definitions.asm"
.include "utils.asm"
.include "constants.asm"

.dseg
state: .byte 1
cin: .byte 1

.cseg
.org 0
		rjmp reset
.org INT7addr
		rjmp handle_remote

.include "lcd.asm"
.include "printf.asm"

reset:	LDSP RAMEND

		SET_VAR_IM state, state_closed
		SET_VAR_IM cin, NOCODE

		OUTI DDRB, 0xff
		OUTI PORTB, 0x00


		OUTI EIMSK, (1 << INT7)

		rcall LCD_init
		rcall LCD_clear
		sei
		rjmp main

.equ T1 = 1778

handle_remote:
		in _sreg, SREG
		push w
		CLR2 b1, b0
		ldi b2, 14
		WAIT_US T1/4

hr_loop:P2C PINE, IR
		ROL2 b1, b0
		WAIT_US T1
		DJNZ b2, hr_loop
		
		SET_VAR cin, b0
		pop w
		out SREG, _sreg

		reti

main:	
		GET_VAR w, cin

		JMNPCI bp, btn_validate
		in w, PORTB
		com w
		out PORTB, w

bp:		cli						; idem
		SET_VAR_IM cin, NOCODE	;
		sei						;
		rjmp main

